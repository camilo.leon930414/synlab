import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
var CryptoJS = require("crypto-js");

@Injectable({
    providedIn: 'root'
})
export class ServicesService {
    constructor(
        private http: HttpClient,
        private router: Router,) {
    }
    public Api = environment.apiUrl;
    public httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': 'my-auth-token',
            'Cache-Control': 'no-cache , no-store, must-revalidate',
            "Pragma": "no-cache",
        })
    }
    login(params: any) {
        let urlApi = this.Api + "/login";
        const paramsSecurity = this.set(params);
        return this.http.post(urlApi, { params: paramsSecurity }, this.httpOptions);
    }
    logout() {
        sessionStorage.removeItem('user');
        this.router.navigate(['/login']);
    }

    dinamicSpPost(params: any, SpName: any) {
        const paramsSecurity = this.set(params);
        return this.http.post(this.Api + '/putDinamicCallSPs', { paramsSQL: paramsSecurity, SpName: SpName }, this.httpOptions);
    }

    dinamicSpGet(param: any, SpName: any) {
        let string = JSON.stringify({ param: param, SpName: SpName });
        return this.http.get(this.Api + '/putDinamicCallSPs/' + string, this.httpOptions);
    }

    sendEmail(data: any) {
        return this.http.post(this.Api + '/send', { dataEmail: data }, this.httpOptions);
    }
    /* methods encrypt and decrypt */
    //The set method is use for crypt the value.
    set(value: any) {
        var key = CryptoJS.enc.Utf8.parse(environment.secretKey);
        var iv = CryptoJS.enc.Utf8.parse(environment.IV);
        if (value != null && value.length > 0) {
            value.forEach((param: any) => {
                Object.keys(param).forEach((index: any) => {
                    if (param[index] != null && param[index] != undefined) {
                        var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(param[index].toString()), key,
                            {
                                keySize: 128 / 8,
                                iv: iv,
                                mode: CryptoJS.mode.CBC,
                                padding: CryptoJS.pad.Pkcs7
                            });
                        param[index] = encrypted.toString()
                    }
                });
            });
            return value;
        } else {
            return null;
        }
    }
    //The get method is use for decrypt the value.
    get(value: any) {
        var key = CryptoJS.enc.Utf8.parse(environment.secretKey);
        var iv = CryptoJS.enc.Utf8.parse(environment.IV);
        if (value != null) {
            var decrypted = CryptoJS.AES.decrypt(value, key, {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });
            return decrypted.toString(CryptoJS.enc.Utf8);
        } else {
            return null;
        }
    }
}
