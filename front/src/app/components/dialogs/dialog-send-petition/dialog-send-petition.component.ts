import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { ServicesService } from '../../../services/services.service';

@Component({
    selector: 'app-dialog-send-petition',
    templateUrl: './dialog-send-petition.component.html',
    styleUrls: ['./dialog-send-petition.component.css']
})
export class DialogSendPetitionComponent implements OnInit {

    public option: any;
    public comment: any;
    public dataEmail: any;

    constructor(private service: ServicesService, public dialogRef: MatDialogRef<DialogSendPetitionComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any, private toast: ToastrService) {
        this.option = null;
        this.comment = null;
    }
    ngOnInit(): void {
        this.option = this.data.state;
        this.dataEmail = this.data.data;
    }

    sendResponse() {
        if ((this.comment != null && this.comment != '') || this.option == 2 || this.option == 6) {

            /* send Email */
            let dataUser: any = sessionStorage.getItem('user');
            dataUser = dataUser != null ? JSON.parse(dataUser) : null;
            if (dataUser == null) { this.toast.error('Error the data', 'Error'); this.service.logout(); }
            Object.keys(dataUser).forEach(index => {
                dataUser[index] = this.service.get(dataUser[index]);
            });
            let dataemailSend = {
                name: this.dataEmail.firstname + ' ' + this.dataEmail.lastname,
                comment: this.comment,
                userResponse: dataUser.firstname + ' ' + dataUser.lastname,
                email: this.dataEmail.email,
                status: this.option == 4 ? 'Approved' : this.option == 3 ? 'No Approved' : this.option == 5 ? 'Warranty Comment' : this.option == 6 ? 'Warranty closed' : 'Process'
            }
            const locationsSubscription = this.service.sendEmail(dataemailSend).subscribe();
            this.toast.success('Email Send to ' + this.dataEmail.email, 'Correct')
            setTimeout(() => {
                this.option = true;
                document.getElementById('btnSend')?.click();
            }, 2000)
            setTimeout(() => {
                locationsSubscription.unsubscribe();
            }, 10000);
        } else {
            this.toast.warning('you must send a comment', 'Warning');
        }
    }
    cancelar() {
        this.dialogRef.close();
    }
}
