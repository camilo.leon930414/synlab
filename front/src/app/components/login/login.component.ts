import { Component } from '@angular/core';
import { ServicesService } from '../../services/services.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent  {
    public user: any;
    public pass: any;
    public message: any;
    public isError: boolean;

    constructor(
        private api: ServicesService,
        private router: Router,
        private toast: ToastrService) {
        this.user = '1030627777';
        this.pass = '123456';
        this.message = null;
        this.isError = false;
    }

    login() {
        if (this.pass == null || this.user == null) {
            this.toast.warning("You must enter Username and Password", "Warning");
            return;
        }
        const Logindata = [
            {
                spName: 'user',
                spParam: this.user,
                type: 'VarChar'
            },
            {
                spName: 'pass',
                spParam: btoa(this.pass),
                type: 'VarChar'
            },
        ];
        this.api.login(Logindata).subscribe((response: any) => {
            let res = response.data;
            res = this.api.get(res);
            res = res != null ? JSON.parse(res) : res;
            if (response.code == 200) {
                let data = res[0];
                this.getUser(data);
                this.router.navigate(['/home']);
                this.toast.success("LogIn Correct", "Success");
            } else {
                this.toast.warning("Invalid Credentials", "Warning");
                setTimeout(() => {
                    window.location.reload();
                }, 1000);
            }
        });
    }

    getUser(data: any) {
        let dataUser = this.api.set([data]);
        sessionStorage.setItem('user', JSON.stringify(dataUser[0]));
    }
}
