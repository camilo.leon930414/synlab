import { Component, OnInit, ViewChild } from '@angular/core';
import { ServicesService } from '../../services/services.service';
import { MatTableDataSource } from '@angular/material/table';
import { ToastrService } from 'ngx-toastr';
import { MatPaginator } from '@angular/material/paginator';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    public task: any;
    public total: any;
    public success: any;
    public columnas: string[];
    public dataSource: any;
    public stateCheck: boolean;
    public length: any;
    @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

    constructor(private service: ServicesService, private toast: ToastrService) {
        this.task = null;
        this.columnas = ['check', 'id', 'name', 'state', 'actions'];
        this.dataSource = [];
        this.stateCheck = false;
        this.length = 0;
        this.total = 0;
        this.success = 0;
    }

    ngOnInit(): void {
        this.getData();
    }

    getData() {
        const Logindata = [
            {
                spName: 'id',
                spParam: 1,
                type: 'Int'
            }
        ];
        const SpName = 'list_task_by_user';
        this.service.dinamicSpPost(Logindata, SpName).subscribe((response: any) => {
            let res = response.data;
            res = this.service.get(res);
            res = res != null ? JSON.parse(res) : res;
            if (response.code == 200) {
                let data = res;
                if (data.length) {
                    data.forEach((item: any) => {
                        item.check = false;
                    });
                    this.dataSource = new MatTableDataSource(data);
                    this.dataSource.paginator = this.paginator;
                    this.length = data.length;
                    this.logic();
                    this.toast.success("Exist Task", "Success");
                } else {
                    this.toast.warning("Not Exist Task", "Warning");
                }
            }
        });
    }

    logic() {
        this.total = this.dataSource.data.length;
        let sum: any = 0;
        this.dataSource.data.forEach((item: any) => {
            if (item.id_state == 1) {
                sum++;
            }
        });
        this.success = sum;
    }

    addTask() {
        if (this.task == null || this.task == "") { this.toast.warning("Empty Task", "Warning"); return; }
        const Logindata = [
            {
                spName: 'id_user',
                spParam: 1,
                type: 'Int'
            },
            {
                spName: 'description',
                spParam: this.task,
                type: 'VarChar'
            }
        ];
        const SpName = 'addTask';
        this.service.dinamicSpPost(Logindata, SpName).subscribe((response: any) => {
            let res = response.data;
            res = this.service.get(res);
            res = res != null ? JSON.parse(res) : res;
            if (response.code == 200) {
                this.toast.success("Add Task", "Success");
                setTimeout(() => {
                    window.location.reload();
                }, 1000);
            } else {
                this.toast.error("Not add Task", "Error");
            }
        });
    }

    deleteTask(id: any) {
        const Logindata = [
            {
                spName: 'id_task',
                spParam: id,
                type: 'Int'
            }
        ];
        const SpName = 'delete_task';
        this.service.dinamicSpPost(Logindata, SpName).subscribe((response: any) => {
            let res = response.data;
            res = this.service.get(res);
            res = res != null ? JSON.parse(res) : res;
            if (response.code == 200) {
                this.toast.info("Delete Task", "Info");
                setTimeout(() => {
                    window.location.reload();
                }, 1000);
            } else {
                this.toast.error("Not Delete Task", "Error");
            }
        });
    }

    editTask(id: any, descriptionTask: any) {
        const Logindata = [
            {
                spName: 'id_task',
                spParam: id,
                type: 'Int'
            },
            {
                spName: 'new_description_task',
                spParam: descriptionTask,
                type: 'VarChar'
            }
        ];
        const SpName = 'edit_task';
        this.service.dinamicSpPost(Logindata, SpName).subscribe((response: any) => {
            let res = response.data;
            res = this.service.get(res);
            res = res != null ? JSON.parse(res) : res;
            if (response.code == 200) {
                this.toast.info("Edit Task", "Info");
                setTimeout(() => {
                    window.location.reload();
                }, 1000);
            } else {
                this.toast.error("Not Edit Task", "Error");
            }
        });
    }

    actionMassive(state: any) {
        if (this.stateCheck) {
            if (state == 1) {
                this.dataSource.data.forEach((item: any) => {
                    this.editTask(item.id, item.description_task);
                });
            } else if (state == 2) {
                this.dataSource.data.forEach((item: any) => {
                    this.deleteTask(item.id)
                });
            } else if (state == 3) {
                this.dataSource.data.forEach((item: any) => {
                    this.changeState(item.id, 1)
                });
            } else {
                this.dataSource.data.forEach((item: any) => {
                    this.changeState(item.id, 2)
                });
            }
        }
    }
    checkAll() {
        if (this.dataSource.data.length) {
            this.dataSource.data.forEach((item: any) => {
                item.check = this.stateCheck ? false : true;
            });
            this.stateCheck = !this.stateCheck ? false : true;
        }
    }

    filtrar(event: Event) {
        const filtro = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filtro.trim().toLowerCase();
    }

    changeState(id: any, state: any) {
        const Logindata = [
            {
                spName: 'idTask',
                spParam: id,
                type: 'Int'
            },
            {
                spName: 'state',
                spParam: state,
                type: 'Int'
            }
        ];
        const SpName = 'change_state';
        this.service.dinamicSpPost(Logindata, SpName).subscribe((response: any) => {
            let res = response.data;
            res = this.service.get(res);
            res = res != null ? JSON.parse(res) : res;
            if (response.code == 200) {
                this.toast.info("Changed State Task", "Info");
                setTimeout(() => {
                    window.location.reload();
                }, 1000);
            } else {
                this.toast.error("Not Change Task", "Error");
            }
        });
    }
}
